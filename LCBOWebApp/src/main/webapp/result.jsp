<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<center>
<div class="container">
	<div class="page-header">
<h1>
    Available Brands
</h1>
</div>
<%
List result= (List) request.getAttribute("brands");
Iterator it = result.iterator();
out.println("<br>At LCBO, We have <br><br>");
while(it.hasNext()){
out.println(it.next()+"<br>");
}
%>
<br>
<br>
<a href="index.html" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Back</a>
</div>
</body>
</html>